<!DOCTYPE html>
<html><!--<![endif]-->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>The Spot - co-working space in Marrakech</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"><meta name="description" content="" /><meta name="keywords" content="" /><meta name="author" content="" />
	<link href="favicon/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57" />
	<link href="favicon/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60" />
	<link href="favicon/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72" />
	<link href="favicon/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
	<link href="favicon/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114" />
	<link href="favicon/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
	<link href="favicon/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144" />
	<link href="favicon/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
	<link href="faviconfaviconfavicon/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
	<link href="faviconfavicon/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png" />
	<link href="favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png" />
	<link href="faviconfavicon/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png" />
	<link href="favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png" />
	<link href="/manifest.json" rel="manifest" /><meta name="msapplication-TileColor" content="#ffffff"><meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png"><meta name="theme-color" content="#ffffff"><!-- Facebook and Twitter integration --><meta property="og:title" content="" /><meta property="og:image" content="" /><meta property="og:url" content="" /><meta property="og:site_name" content="" /><meta property="og:description" content="" /><meta name="twitter:title" content="" /><meta name="twitter:image" content="" /><meta name="twitter:url" content="" /><meta name="twitter:card" content="" /><!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link href="ffavicon.ico" rel="shortcut icon" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700" rel="stylesheet" type="text/css" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /><!-- Animate.css -->
	<link href="css/animate.css" rel="stylesheet" /><!-- Icomoon Icon Fonts-->
	<link href="css/icomoon.css" rel="stylesheet" /><!-- Simple Line Icons -->
	<link href="css/simple-line-icons.css" rel="stylesheet" /><!-- Magnific Popup -->
	<link href="css/magnific-popup.css" rel="stylesheet" /><!-- Bootstrap  -->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<meta name="google-site-verification" content="" />
<script src='https://www.google.com/recaptcha/api.js'></script>

	<link href="css/style.css" rel="stylesheet" /><!-- Styleswitcher ( This style is for demo purposes only, you may delete this anytime. ) -->
	<link href="css/style.css" id="theme-switch" rel="stylesheet" /><!-- End demo purposes only --><!-- Modernizr JS --><script src="js/modernizr-2.6.2.min.js"></script><!-- FOR IE9 below --><!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<header id="fh5co-header" role="banner">
<div class="container"><!-- <div class="row"> -->
<nav class="navbar navbar-default">
<div class="navbar-header"><!-- Mobile Toggle Menu Button --><a class="navbar-brand" href="index.html"><img alt="" class="brand-logo" src="./images/logo_color.png" /> </a></div>

<div class="navbar-collapse collapse" id="navbar">
<ul class="nav navbar-nav navbar-right">
	<li class="active"><a data-nav-section="home" href="#"><span>Home</span></a></li>
	<li><a data-nav-section="about" href="#"><span>About</span></a></li>
	<li><a data-nav-section="services" href="#"><span>Services</span></a></li>
	<li><a data-nav-section="packages" href="#"><span>Packages</span></a></li>
	<li><a data-nav-section="gallery" href="#"><span>Gallery</span></a></li>
	<li><a data-nav-section="testimonials" href="#"><span>Testimonials</span></a></li>
	<li><a data-nav-section="contact" href="#"><span>Contact</span></a></li>
</ul>
</div>
</nav>
<!-- </div> --></div>
</header>

<section data-section="home" data-stellar-background-ratio="0.5" id="fh5co-home" style="">
<div class="gradient"></div>

<div class="container">
<div class="text-wrap">
<div class="text-inner">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<h1 class="to-animate cta-text ">Nothing great is ever achieved alone!</h1>

<h2 class="to-animate">Join the community</h2>
<!-- Button trigger modal --><button class="btn btn-primary btn-lg" data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>
</div>
</div>
</div>

<div class="slant"></div>
</section>

<section id="fh5co-intro">
<div class="container">
<div class="row row-bottom-padded-lg">
<div class="fh5co-block to-animate" style="background-image: url(images/img_7.jpg);">
<div class="overlay-darker"></div>

<div class="overlay"></div>

<div class="fh5co-text">
<h2>Co-working</h2>

<p>A get together spot for skilled &amp; innovative young enterpreneurs, &amp; freelancers.</p>
</div>
</div>

<div class="fh5co-block to-animate" style="background-image: url(images/img_10.jpg);">
<div class="overlay-darker"></div>

<div class="overlay"></div>

<div class="fh5co-text">
<h2>Community</h2>

<p>Nothing great is ever achieved alone! Join the community.</p>
</div>
</div>

<div class="fh5co-block to-animate" style="background-image: url(images/img_8.jpg);">
<div class="overlay-darker"></div>

<div class="overlay"></div>

<div class="fh5co-text">
<h2>Creativity</h2>

<p>A spot to enhance your creativity through culture, art &amp; entertainment.</p>
</div>
</div>
</div>
</div>
</section>

<section data-section="about" id="fh5co-about">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center">
<h2 class="to-animate">About</h2>

<div class="row">
<div class="col-md-8 col-md-offset-2 subtext to-animate">
<h3>HELLO, IS IT ME YOU&#39;RE LOOKING FOR?</h3>
</div>
</div>
</div>
</div>

<p class="about-subtext">The Spot is a community centre designed to be a beautiful space for start-up businesses, freelancers, entrepreneurs, art lovers and everyone else in Marrakech. The Spot strives to push you to achieve your full creative and productive potentials. We believe our co-working space, fun zone and collaborative organizations provide the foundation for people and the community to do their best work. Located at the heart of Marrakech city, The Spot is super easy to access and you are in close proximity to shops, restaurants and other convenience stores.&nbsp; With a relaxed cafe style environment offering open plan hotdesks, very convenient packs and access to a myriad of astonishing events our community centre is a great place to spend your working days surrounded by dynamic start-ups from the creative community. You will also have the opportunity to meet with like-minded people in our fun zone events. The events will be a smooth break for networking and a chance to bring you closer to people and tastes from Marrakech city.</p>

<section data-section="photos" id="">
<div class="container">
<h2>Photos Album</h2>

<div class="row row-bottom-padded-sm">
<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/pic1.jpg"><img alt="Image" class="img-responsive" src="./images/pic1.jpg" /> </a>

<div class="fh5co-text"></div>
<a class="fh5co-project-item image-popup to-animate" href="./images/pic1.jpg"> </a></div>

<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/pic2.jpg"><img alt="Image" class="img-responsive" src="./images/pic2.jpg" /> </a>

<div class="fh5co-text"></div>
<a class="fh5co-project-item image-popup to-animate" href="./images/pic2.jpg"> </a></div>

<div class="clearfix visible-sm-block"></div>

<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/pic3.jpg"><img alt="Image" class="img-responsive" src="./images/pic3.jpg" /> </a>

<div class="fh5co-text"></div>
<a class="fh5co-project-item image-popup to-animate" href="./images/pic3.jpg"> </a></div>
</div>
</div>
</section>
</div>
</section>

<div class="fh5co-overlay"></div>

<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center to-animate">
<h2>Fun Facts</h2>
</div>
</div>

<div class="row">
<div class="col-md-3 col-sm-6 col-xs-6">
<div class="fh5co-counter to-animate"><span class="fh5co-counter-number js-counter" data-from="0" data-refresh-interval="50" data-speed="5000" data-to="89">89</span> <span class="fh5co-counter-label">Finished projects</span></div>
</div>

<div class="col-md-3 col-sm-6 col-xs-6">
<div class="fh5co-counter to-animate"><span class="fh5co-counter-number js-counter" data-from="0" data-refresh-interval="50" data-speed="5000" data-to="2343409">2343409</span> <span class="fh5co-counter-label">Line of codes</span></div>
</div>

<div class="col-md-3 col-sm-6 col-xs-6">
<div class="fh5co-counter to-animate"><span class="fh5co-counter-number js-counter" data-from="0" data-refresh-interval="50" data-speed="5000" data-to="1302">1302</span> <span class="fh5co-counter-label">Cup of coffees</span></div>
</div>

<div class="col-md-3 col-sm-6 col-xs-6">
<div class="fh5co-counter to-animate"><span class="fh5co-counter-number js-counter" data-from="0" data-refresh-interval="50" data-speed="5000" data-to="52">52</span> <span class="fh5co-counter-label">Happy clients</span></div>
</div>
</div>
</div>
<!--	<section class="fh5co-about">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="">Zonee</h2>
				</div>
			</div>
		</div>
		<div class="zones">
			<div class="container text-center">
				<div class="row text-center">
					<div class="desk col-md-5">
						<span class=" text-center zone-title"> The desk</span>
						<p class="text-center zone-descreption">It&#39;s the reception, or you can have all the information about
the services offered, buy accessories, print, book the meeting room.</p>
					</div>
					<div class="blue col-md-5">
						<span class="text-center zone-title">blue Lagoon</span>
						<p class="text-center zone-descreption">these are the actual working positions where you have
access to sockets and wifi –Optical Fiber 100 mega, as well as a meeting
room (on reservation) that can accommodate up to a 28 people, with two
flat screens and a whiteboard so you can make your presentations alive.</p>
					</div>
					<div class="red col-md-5">
						<span class="text-center zone-title">Red carpet</span>
						<p class="text-center zone-descreption"></p>
					</div>
					<div class="yellow col-md-5">
						<span class="text-center zone-title">Yellow corner</span>
						<p class="text-center zone-descreption"></p>
					</div>
					<div class="green col-md-10">
						<span class="text-center zone-title">Green zone</span>
						<p class="text-center zone-descreption"></p>
					</div>
				</div>
			</div>
		</div>
	</section>-->

<section data-section="services" id="fh5co-services">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-left">
<h2 class="left-border to-animate">Services</h2>

<div class="row">
<div class="col-md-8 subtext to-animate">
<h3>We are currently working on some extended offerings and partnerships but our members have access to printing facilities, courier services, virtual assistants, food from Warm &amp; Glad restaurant, an in-house tuck shop +more. But for now, this is what we can offer you just for using our space.... I know, right?!?</h3>
</div>
</div>
</div>
</div>

<div class="row text-center">
<div class="col-md-3 col-sm-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/015-wifi.png" />
<h3>WIFI</h3>
</div>

<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/014-phone-call.png" />
<h3>Landline</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/013-books.png" />
<h3>Participative library</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/012-open-24-hours.png" />
<h3>24 X 7</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/011-cleaning.png" />
<h3>Daily cleaning</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/holidays.png" />
<h3>Front desk</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/009-printer.png" />
<h3>Printer</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/001-content.png" />
<h3>Web design</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/007-confetti.png" />
<h3>Fun zone</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/006-calendar.png" />
<h3>Events</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/008-office-scanner.png" />
<h3>Scanner</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/005-bicycle.png" />
<h3>Bikes</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/004-lockers.png" />
<h3>Lockers</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/gamepad.png" />
<h3>Gaming</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/meeting.png" />
<h3>Meeting room</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/desk.png" />
<h3>Hot desk</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/003-breakfast.png" />
<h3>Fooding</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/002-scissors.png" />
<h3>Discounts</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/001-parking.png" />
<h3>Parking</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/cupcake.png" />
<h3>Bakery</h3>

<p class="soon">soon</p>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/hammock-relaxing.png" />
<h3>Nap space</h3>

<p class="soon">soon</p>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/paint.png" />
<h3>art</h3>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images//shower.png" />
<h3>Shower</h3>

<p class="soon">soon</p>
</div>

<div class="col-md-3 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/001-lemonade.png" />
<h3>Drinks</h3>
</div>

<div class="col-md-12 col-sm-6 col-xs-6 fh5co-service to-animate"><img alt="" class="img-icon" src="./images/002-laptop.png" />
<h3>Web development</h3>
</div>
</div>
</div>
</section>

<section data-section="pricing" id="pricing"><!-- pricing-->
<section data-section="packages" id="pricing">
<div class="oukrims-pricing">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center">
<h2>Packages</h2>

<div class="row">
<div class="col-md-8 col-md-offset-2 subtext">
<h3>Choose from one of our well crafted packages and let the fun begin.&nbsp;We can&#39;t wait to welcome you.</h3>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-3 col-sm-6">
<div class="price-box top-pricing-box">
<h2 class="pricing-plan">Driyef</h2>
<!--	<div class="price">600 dhs * </div>-->

<hr />
<p>Access(3 days/week)</p>

<p>Wifi</p>

<p>Landline</p>

<p>Participative library</p>

<p>Daily cleaning</p>

<p>Front desk</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>

<div class="col-md-3 col-sm-6">
<div class="price-box top-pricing-box">
<h2 class="pricing-plan">L9afeze</h2>
<!--	<div class="price">1100 dhs *</div>-->

<hr />
<p class="previous-b">All Driyef benefits +</p>

<p>Unlimitted access</p>

<p>Printer</p>

<p>Fun zone</p>

<p>Events</p>

<p>Scanner</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>

<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6">
<div class="price-box top-pricing-box">
<h2 class="pricing-plan">M3alem</h2>
<!--<div class="price">
									1600 dhs *
								</div>-->

<hr />
<p class="previous-b">All L9afez benefits +</p>

<p>Unlimitted access</p>

<p>Bikes</p>

<p>Locker</p>

<p>Gaming</p>

<p>Meeting room</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>

<div class="col-md-3 col-sm-6">
<div class="price-box top-pricing-box">
<h2 class="pricing-plan">ElPatrone</h2>
<!--	<div class="price">
									2800 dhs *
								</div>-->

<hr />
<p class="previous-b">M3alem benefits +</p>

<p>Unlimitted access</p>

<p>Hot desk</p>

<p>Breakfast</p>

<p>Workshop discount</p>

<p>24/7</p>

<p>Parking</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>

<div class="bottom-pricing ">
<div class="col-md-3 col-sm-6 col-xs-12">
<div class="price-box bottom-pricing-box">
<h2 class="pricing-plan">Day pass</h2>
<!--<div class="price">
									 50 dhs *
								 </div>-->

<hr />
<p>Access (9 a.m. to 9 p.m.)</p>

<p>Wifi</p>

<p>Front Desk</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>

<div class="col-md-3 col-sm-6 col-xs-12">
<div class="price-box bottom-pricing-box">
<h2 class="pricing-plan">Week Pass</h2>
<!--	<div class="price">
									 300 dhs *
								 </div>-->

<hr />
<p>One week access</p>

<p>Wifi</p>

<p>Front Desk</p>

<ul class="pricing-info">
</ul>
<button class="btn btn-default " data-target="#myModal" data-toggle="modal" type="button">Book a tour !</button></div>
</div>
</div>
</div>

<div class="note"></div>
</div>
</div>
</section>
</section>

<section data-section="gallery" id="fh5co-work">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center">
<h2 class="to-animate">Art Gallery</h2>

<div class="row">
<div class="col-md-8 col-md-offset-2 subtext to-animate">
<h3>The Spot dedicates one of its corners to Artists and art lovers. Monthly art exhibitions and artistic meetings will be organized. The gallery is open to all talented artists. To be featured, please send us a portfolio and a short description of your work. Our team will contact you should you be selected.</h3>
</div>
</div>
</div>
</div>

<div class="row row-bottom-padded-sm">
<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/gallery_1.jpg"><img alt="Image" class="img-responsive" src="./images/gallery_1.jpg" /> </a></div>

<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/gallery_2.jpg"><img alt="Image" class="img-responsive" src="./images/gallery_2.jpg" /> </a></div>

<div class="clearfix visible-sm-block"></div>

<div class="col-md-4 col-sm-6 col-xxs-12"><a class="fh5co-project-item image-popup to-animate" href="./images/gallery_3.jpg"><img alt="Image" class="img-responsive" src="./images/gallery_3.jpg" /> </a></div>
</div>
</div>
</section>

<section data-section="testimonials" id="fh5co-testimonials">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center">
<h2 class="to-animate">Testimonials</h2>

<div class="row">
<div class="col-md-8 col-md-offset-2 subtext to-animate">
<h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, here is what people feel about us .</h3>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-4">
<div class="box-testimony">
<blockquote class="to-animate-2">
<p>&ldquo;I want everyone to know about the Youth Spot &ndash; A place for us to share our talents, experiences, passion, hopes, good memories and connect with each other. Marrakech got us the place, It&rsquo;s time to make its history .&rdquo;</p>
</blockquote>

<div class="author to-animate">
<figure><img alt="Person" src="https://scontent-mrs1-1.xx.fbcdn.net/v/t1.0-1/14441084_1208274832561903_8930835035683675572_n.jpg?oh=f6c867bec72de19fa71b4ccc6441d21a&amp;oe=5A00FF67" /></figure>

<p>Hanane Nabil Ait Ouakrim</p>
</div>
</div>
</div>

<div class="col-md-4">
<div class="box-testimony">
<blockquote class="to-animate-2">
<p>&ldquo;A good place to work with your team , I liked the atmosphere and the cool people there.&rdquo;</p>
</blockquote>

<div class="author to-animate">
<figure><img alt="Person" src="https://scontent-mrs1-1.xx.fbcdn.net/v/t1.0-9/19642373_861280974037929_2445780650732263373_n.jpg?oh=5d9875aea39d0c6cd49c4d069341cce7&amp;oe=5A01F06D" /></figure>

<p>Salah oukrim</p>
</div>
</div>
</div>

<div class="col-md-4">
<div class="box-testimony">
<blockquote class="to-animate-2">
<p>&ldquo;The new Spot location is excellent with a terrific view. Super fast internet! the Spot is the spot to be if you want to work or study in a productive environment. &rdquo;</p>
</blockquote>

<div class="author to-animate">
<figure><img alt="Person" src="https://scontent-mrs1-1.xx.fbcdn.net/v/t1.0-1/20031577_1358327990952241_7548621816573096950_n.jpg?oh=b3300ff41d01567c2c325d53078b7c60&amp;oe=5A2BB3BA" /></figure>

<p>Sahar Elhallak</p>
</div>
</div>
</div>
</div>
</div>
</section>

<section data-section="contact" id="fh5co-contact">
<div class="container">
<div class="row">
<div class="col-md-12 section-heading text-center">
<h2 class="to-animate">Get In Touch</h2>

<div class="row">
<div class="col-md-8 col-md-offset-2 subtext to-animate">
<h3>you will have unlimited access to an incredibly vibrant community and meet other like-minded people.</h3>
</div>
</div>
</div>
</div>

<div class="row row-bottom-padded-md">
<div class="col-md-6 to-animate">
<h3>Contact Info</h3>

<ul class="fh5co-contact-info">
	<li class="fh5co-contact-address ">N&deg; 33, Rue Yugoslavie,<br />
	Marrakech, Maroc</li>
	<li>0666190187<br />
	0661757057</li>
	<li>hello@thespot.ma</li>
	<li><a href="http://thespot.ma/" target="_blank">thespot.ma</a></li>
</ul>
</div>

<form action="mail.php" class="col-md-6 to-animate" method="post">
<h3>Contact Form</h3>

<div class="form-group "><label class="sr-only" for="name">Name</label> <input class="form-control" id="name" name="name" placeholder="Name" required="" type="text" /></div>

<div class="form-group "><label class="sr-only" for="email">Email</label> <input class="form-control" id="email" name="email" placeholder="Email" required="" type="email" /></div>

<div class="form-group "><label class="sr-only" for="phone">Phone</label> <input class="form-control" id="phone" name="phone" placeholder="Phone" required="" type="number" /></div>

<div class="form-group "><label class="sr-only" for="message">Message</label><textarea class="form-control" cols="30" id="message" name="message" placeholder="Message" rows="5"></textarea></div>



<div class="form-group "><input class="btn btn-primary btn-lg" name="submit" type="submit" value="Send Message" /></div>

</form>
</div>
</div>

<div class="to-animate" id="map"></div>
</section>

<footer id="footer" role="contentinfo">
<div class="container">
<div>
<div class="col-md-12 text-center">Copyright (c) 2017 All Rights Reserved to the awesome spot.</div>
</div>

<div class="row">
<div class="col-md-12 text-center">
<ul class="social social-circle">
	<li></li>
	<li></li>
	<li></li>
</ul>
</div>
</div>
</div>
</footer>
<!-- Modal -->

<div aria-labelledby="myModalLabel" class="modal fade" id="myModal" role="dialog" tabindex="-1">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header"><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>

<h4 class="modal-title" id="myModalLabel">Book a tour</h4>
</div>

<div class="modal-body"><iframe allowtransparency="true" class="bookingIframe" frameborder="0" id="ycbmiframethespotmarrakech" src="https://thespotmarrakech.youcanbook.me/?noframe=true&amp;skipHeaderFooter=true" style="width:100%;height:750px;border:0px;background-color:transparent;"></iframe><script>window.addEventListener && window.addEventListener("message", function(event){if (event.origin === "https://thespotmarrakech.youcanbook.me"){document.getElementById("ycbmiframethespotmarrakech").style.height = 750 + "px";}}, false);</script></div>

<div class="modal-footer"><button class="btn btn-default" data-dismiss="modal" type="button">Close</button></div>
</div>
</div>
</div>
<style type="text/css">.poweredByFooter{
		opacity: 0;
		display: none !important;
	}
</style>
<!-- jQuery --><script src="js/jquery.min.js"></script><!-- jQuery Easing --><script src="js/jquery.easing.1.3.js"></script><!-- Bootstrap --><script src="js/bootstrap.min.js"></script><!-- Waypoints --><script src="js/jquery.waypoints.min.js"></script><!-- Stellar Parallax --><script src="js/jquery.stellar.min.js"></script><!-- Counter --><script src="js/jquery.countTo.js"></script><!-- Magnific Popup --><script src="js/jquery.magnific-popup.min.js"></script><script src="js/magnific-popup-options.js"></script><!-- Google Map --><script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD94zXvD-4B-yyJz70qcjVpw-uNlUyrRwE"></script><script src="js/google_map.js"></script><!-- Main JS (Do not remove) --><script src="js/main.js"></script></body>
</html>