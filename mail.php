<?php



        //vars

        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $message = $_POST['message'];

        //the spot mail

        $to = "hello@thespot.ma";

        $subject = "hello from the spot website";

        $mailContent = $message."   \n\n this my phone ".$phone."  \n\n just in case this my email ".$email."\n\n  best regards ".$name;


        $headers = "from : ".$email;
                mail($to,$subject,$mailContent,$headers);


 ?>

 <!DOCTYPE html>
 <html>
 <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>the spot &mdash; co-working sapce in Marrakech </title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="description" content="" />
     <meta name="keywords" content="" />
     <meta name="author" content="" />


     <!-- Facebook and Twitter integration -->
     <meta property="og:title" content="" />
     <meta property="og:image" content="" />
     <meta property="og:url" content="" />
     <meta property="og:site_name" content="" />
     <meta property="og:description" content="" />
     <meta name="twitter:title" content="" />
     <meta name="twitter:image" content="" />
     <meta name="twitter:url" content="" />
     <meta name="twitter:card" content="" />

     <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
     <link rel="shortcut icon" href="favicon.ico">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>

     <!-- Animate.css -->
     <link rel="stylesheet" href="css/animate.css">
     <!-- Icomoon Icon Fonts-->
     <link rel="stylesheet" href="css/icomoon.css">
     <!-- Simple Line Icons -->
     <link rel="stylesheet" href="css/simple-line-icons.css">
     <!-- Magnific Popup -->
     <link rel="stylesheet" href="css/magnific-popup.css">
     <!-- Bootstrap  -->
     <link rel="stylesheet" href="css/bootstrap.css">

     <!--
     Default Theme Style
     You can change the style.css (default color purple) to one of these styles

     1. pink.css
     2. blue.css
     3. turquoise.css
     4. orange.css
     5. lightblue.css
     6. brown.css
     7. green.css

     -->
     <link rel="stylesheet" href="css/style.css">

     <!-- Styleswitcher ( This style is for demo purposes only, you may delete this anytime. ) -->
     <link rel="stylesheet" id="theme-switch" href="css/style.css">
     <!-- End demo purposes only -->




     <!-- Modernizr JS -->
     <script src="js/modernizr-2.6.2.min.js"></script>
     <!-- FOR IE9 below -->
     <!--[if lt IE 9]>
     <script src="js/respond.min.js"></script>
     <![endif]-->

 </head>

     <body>
         <section id="fh5co-home" data-section="home" style="" data-stellar-background-ratio="0.5">
             <div class="gradient"></div>
             <div class="container">
                 <div class="text-wrap">
                     <div class="text-inner">
                         <div class="row">
                             <div class="col-md-8 col-md-offset-2">
                                 <h1 class="cta-text ">Message sent !</h1>
                                 <h2 class="">
                                    <a href="http://thespot.ma"> Back to the home page</a></h2>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="slant"></div>
         </section>
     </body>
 </html>
